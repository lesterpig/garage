# Summary

[The Garage Data Store](./intro.md)

- [Getting Started](./getting_started/index.md)
  - [Get a binary](./getting_started/binary.md)
  - [Configure the daemon](./getting_started/daemon.md)
  - [Control the daemon](./getting_started/control.md)
  - [Configure a cluster](./getting_started/cluster.md)
  - [Create buckets and keys](./getting_started/bucket.md)
  - [Handle files](./getting_started/files.md)

- [Cookbook](./cookbook/index.md)
  - [Host a website](./cookbook/website.md)
  - [Integrate as a media backend]()
  - [Operate a cluster]()
  - [Recovering from failures](./cookbook/recovering.md)

- [Reference Manual](./reference_manual/index.md)
  - [Garage CLI]()
  - [S3 API](./reference_manual/s3_compatibility.md)

- [Design](./design/index.md)
  - [Related Work](./design/related_work.md)
  - [Internals](./design/internals.md)

- [Development](./development/index.md)
  - [Setup your environment](./development/devenv.md)
  - [Your first contribution]()

- [Working Documents](./working_documents/index.md)
  - [Load Balancing Data](./working_documents/load_balancing.md)
